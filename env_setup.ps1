# 1. Install Terminal (Microsoft Store)
# 2. Set-ExecutionPolicy unrestricted
# 2. Set-ExecutionPolicy Bypass -Scope Process -Force
# 2. Set-ExecutionPolicy RemoteSigned -Scope CurrentUser
Write-Host "=== Install chocolately..."
Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://community.chocolatey.org/install.ps1'))

Write-Host "=== Creating your general development environment!..."
choco install openvpn -y
choco install unikey -y
choco install winrar -y
choco install git -y
choco install gitextensions -y
choco install curl -y
choco install wget -y
choco install python3 -y
choco install terraform -y
choco install openssl -y
choco install dotnetcore-sdk -y
choco install visualstudio2019buildtools -y
choco install VirtualMachinePlatform -y -source windowsfeatures
choco install Microsoft-Windows-Subsystem-Linux -y -source windowsfeatures
choco install wsl2 --params "/Version:2 /Retry:true" -y
choco install wsl-ubuntu-2004 --params "/InstallRoot:true" -y
choco install docker-desktop -y
choco install docker-compose -y
choco install docker-cli -y
choco install mysql.workbench --version=8.0.26 -y
choco install lightshot.install -y
choco install discord -y
choco install telegram.install -y
choco install screentogif -y
choco install pandoc -y

Write-Host "====> Installing IDEs..."
choco install vscode -y

Write-Host "====> Installing web browsers..."
choco install GoogleChrome -y

Write-Host "====> Installing api tools..."
choco install postman -y

Write-Host "====> Installing communication tools..."
choco install slack -y
choco install zalopc -y

Write-Host "====> Installing windows subsystem for linux..."
choco install wsl2 --params "/Version:2 /Retry:true" -y
choco install wsl-ubuntu-2004 --params "/InstallRoot:true" -y

Write-Host "=== Creating your nodejs development environment!..."
choco install nodejs-lts -y
npm install pm2@latest -g
npm install yarn -g

Write-Host "=== Creating your php 7.3.6 development environment!..."
choco install php --version=7.3.6 -y
choco install composer -y

Write-Host "=== Creating your blockchain development environment!..."
# choco install ganache -y

Write-Host "=== Creating your game development environment!..."
# choco install blender -y

Write-Host "=== Creating your virtualbox!..."
# choco install virtualbox -y

Write-Host "=== Creating your android development environment!..."
# choco install -y qt5-default qtcreator
# choco install -y openjdk8 -y
# choco install android-sdk -y
# # sdkmanager --update
# # sdkmanager --licenses
# choco install androidstudio -y

Write-Host "=== Your development environment is ready to use! Enjoy! ==="
